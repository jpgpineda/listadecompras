//
//  ListItemsViewController.swift
//  ListaDeCompras
//
//  Created by javier pineda on 15/12/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ListItemsViewController: UIViewController {

    @IBOutlet weak var mainTable: UITableView!
    @IBOutlet weak var titleBar: UINavigationItem!
    var ref: DatabaseReference!
    var products = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backgroundColor = .orange
        
        ref = Database.database().reference(withPath: "list-items")
        loadDataFromFireBase()
        
        mainTable.dataSource = self
        mainTable.delegate = self
    }

    func loadDataFromFireBase(){
        ref.queryOrdered(byChild: "completed").observe(.value) { (snapShot, value) in
            self.products = [Product]()
            for item in snapShot.children{
                let dataSnapShot = item as! DataSnapshot
                let product = Product(snapShot: dataSnapShot)
                
                self.products.append(product)
            }
            self.mainTable.reloadData()
        }
    }
    @IBAction func singOut(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Atencion!!", message: "Deseas cerrar sesion?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Si", style: .default, handler: { (action) in
            do{
                try Auth.auth().signOut()
                self.navigationController?.popViewController(animated: true)
            }catch let error{
                self.showErrorAlert(error.localizedDescription)
            }
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addProductToList(_ sender: UIBarButtonItem) {
        
        let alert = UIAlertController(title: "Lista de compras", message: "Agrega un elemento", preferredStyle: .alert)
        alert.addTextField { (textFieldName) in
            textFieldName.placeholder = "agrega el nombre del producto"
            
        }
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Guardar", style: .default, handler: { (action) in
            
            guard let textFields = alert.textFields else { return }
            let textFieldName = textFields.first
            if textFieldName!.text!.count > 0{
                self.saveProductInFireBase(productName: textFieldName!.text!)
            }
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    //agregar un registro a la base de datos
    func saveProductInFireBase(productName: String){
        guard let user = Auth.auth().currentUser else { return }
        let productRef =  ref.child(productName.lowercased())
        
        let values = ["name": productName,
                      "user": user.email!,
                      "completed": false] as [String : Any]
        
        productRef.setValue(values)
    }
    
}

extension ListItemsViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let idCell = "idCell"
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: idCell)
        
        let product = products[indexPath.row]
        cell.textLabel!.text = product.name
        cell.detailTextLabel!.text = product.user
        cell.textLabel!.textColor = .white
        cell.detailTextLabel!.textColor = .white
        
        changeStatusCell(cell, isCompleted: product.completed)
        
        return cell
    }
}

extension ListItemsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let product = products[indexPath.row]
        let completed = !product.completed
        //Actualizar propiedad de firebase
        guard let ref = product.ref else {return}
        ref.updateChildValues(["completed" : !product.completed])
        
        guard let cell = tableView.cellForRow(at: indexPath) else {return}
        changeStatusCell(cell, isCompleted: product.completed)
    }
    
    func changeStatusCell(_ cell: UITableViewCell, isCompleted: Bool){
        if isCompleted{
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = .gray
            cell.detailTextLabel?.textColor = .gray
        }else{
            cell.accessoryType = .none
            cell.textLabel?.textColor = .black
            cell.detailTextLabel?.textColor = .black
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            let product = products[indexPath.row]
            //eliminar registro de firebase
            guard let ref = product.ref else {return}
            ref.removeValue()
        }
    }
}

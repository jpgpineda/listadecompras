//
//  LoginViewController.swift
//  ListaDeCompras
//
//  Created by javier pineda on 15/12/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnLogin.layer.cornerRadius = 10
        btnLogin.clipsToBounds = true
        
        if let userEmail = UserDefaults.standard.string(forKey: "kLastUser"){
            txtUser.text = userEmail
        }
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil{
                self.performSegue(withIdentifier: "showhome", sender: nil)
            }
        }
        
        //Auth.auth().signOut()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func login(_ sender: UIButton) {
        handlerSingIn(txtUser.text!, txtPassword.text!)
    }
    
    @IBAction func singUp(_ sender: UIButton) {
        performSegue(withIdentifier: "showRegister", sender: nil)
        
    }
    
    @IBAction func closeKeyBoard(_ sender: UIControl){
        view.endEditing(true)
    }
    func handlerSingIn(_ userEmail: String, _ userPassword: String){
        Auth.auth().signIn(withEmail: userEmail, password: userPassword) { (result, error) in
            if error == nil{
                UserDefaults.standard.set(userEmail, forKey: "kLastUser")
                self.txtPassword.text = ""
                //self.performSegue(withIdentifier: "showListItems", sender: nil)
            }else{
                self.showErrorAlert(error!.localizedDescription)
            }
        }
    }
    
}

extension UIViewController{
    
    func showErrorAlert(_ message: String){
        let alert = UIAlertController(title: "Ups", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func showConfimation(title: String,
                         message: String,
                         cancel: String?,
                         confirm: String,
                         confirmAction:  @escaping() -> Void) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        if let cancel = cancel {
            alert.addAction(UIAlertAction(title: cancel,
                                          style: .cancel,
                                          handler: nil))
        }
        
        alert.addAction(UIAlertAction(title: confirm,
                                      style: .default,
                                      handler: { (action) in
                                        
                                        confirmAction()
                                        
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    func showInfo(title: String,
                  message: String,
                  confirm: String) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: confirm,
                                      style: .default,
                                      handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
}

extension LoginViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtUser:
            txtPassword.becomeFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

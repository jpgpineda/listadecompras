//
//  RegisterViewController.swift
//  ListaDeCompras
//
//  Created by javier pineda on 11/18/19.
//  Copyright © 2019 MobileStudio. All rights reserved.
//

import UIKit
import FirebaseAuth

class RegisterViewController: UIViewController {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtNickName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iconImage.layer.cornerRadius = iconImage.frame.size.width / 2
        iconImage.clipsToBounds = true
        btnRegister.layer.cornerRadius = 10
        btnRegister.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func doRegister(_ sender: UIButton) {
        signUp()
    }
    
    @IBAction func closeKeyBoard(_ sender: UIControl){
        view.endEditing(true)
    }
    
    func validateInfo() -> Bool{
        guard txtName.text!.count > 3 else {
            self.showErrorAlert("Ups, el nombre debe ser mayor a 3 caracteres")
            return false
        }
        guard txtLastName.text!.count > 3 else{
            self.showErrorAlert("Ups, el apellido debe ser mayor a 3 caracteres")
            return false
        }
        guard txtNickName.text!.count > 5 else {
            self.showErrorAlert("Ups, el pseudonimo debe ser mayor a 5 caracteres")
            return false
        }
        guard txtEmail.text!.count > 10 else {
            self.showErrorAlert("Ups, el email debe ser mayor a 10 caracteres")
            return false
        }
        guard txtPassword.text!.count > 8 else {
            self.showErrorAlert("Ups, la contraseña debe ser mayor a 8 caracteres")
            return false
        }
        return true
    }
    
    func signUp(){
        if validateInfo() == true{
            let user = User(name: txtName.text!,
                            lastName: txtLastName.text!,
                            nickName: txtNickName.text!,
                            email: txtEmail.text!,
                            password: txtPassword.text!)
            user.registerUser { (succes, error) in
                if error == nil{
                    Auth.auth().createUser(withEmail: user.email, password: self.txtPassword.text!) { (result, error) in
                        if error == nil{
                            self.showInfo(title: "Listo", message: "Tu registro esta completo", confirm: "Ok")
                            self.handlerSingIn(user.email, self.txtPassword.text!)

                        } else {
                            self.showErrorAlert(error!.localizedDescription)
                        }
                    }
                    
                } else{
                    self.showErrorAlert(error!.localizedDescription)
                }
            }
        }
    }
    
    func handlerSingIn(_ userEmail: String, _ userPassword: String){
        Auth.auth().signIn(withEmail: userEmail, password: userPassword) { (result, error) in
            if error == nil{
                self.performSegue(withIdentifier: "showhome", sender: nil)
            }else{
                self.showErrorAlert(error!.localizedDescription)
            }
        }
    }
    

}

extension RegisterViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case txtName:
            txtLastName.becomeFirstResponder()
        case txtLastName:
            txtNickName.becomeFirstResponder()
        case txtNickName:
            txtEmail.becomeFirstResponder()
        case txtEmail:
            txtPassword.becomeFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

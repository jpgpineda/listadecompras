//
//  Product.swift
//  ListaDeCompras
//
//  Created by javier pineda on 15/12/18.
//  Copyright © 2018 MobileStudio. All rights reserved.
//

import Foundation
import FirebaseDatabase

class Product {
    let key: String!
    let name: String!
    let user: String!
    let completed: Bool!
    let ref: DatabaseReference?
    
    init(snapShot: DataSnapshot){
        self.key = snapShot.key
        let dictionary = snapShot.value as! [String: Any]
        self.name = dictionary["name"] as! String
        self.user = dictionary["user"] as! String
        self.completed = dictionary["completed"] as! Bool
        self.ref = snapShot.ref
    }
}

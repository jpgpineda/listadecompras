//
//  User.swift
//  ListaDeCompras
//
//  Created by javier pineda on 11/18/19.
//  Copyright © 2019 MobileStudio. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore

class User{
    let name: String!
    let lastName: String!
    let nickName: String!
    let email: String!
    let password: String!
    
    init(name: String,
         lastName: String,
         nickName: String,
         email: String,
         password: String) {
        self.name = name
        self.lastName = lastName
        self.nickName = nickName
        self.email = email
        self.password = password
    }
    
    func registerUser(completionHandler: @escaping(_ success: Bool, _ error: Error?) -> Void) {
        let db = Firestore.firestore()
        db.collection("Clients").addDocument(data:
            [
                "name" : name,
                "last_name": lastName,
                "nickName": nickName,
                "email": email]
        ) {error in
            if error == nil{
                completionHandler(true, nil)
            }else{
                completionHandler(false, error!)
            }
        }
    }
}
